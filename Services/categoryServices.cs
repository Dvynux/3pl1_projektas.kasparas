﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Models;
using MySql.Data.MySqlClient;

namespace Services
{
    public class categoryServices : BaseService
    {
        public categoryServices()
        {

        }
        public List<Category> GetCategories()
        {
            this.ProcedureName = "Get_Categories";

            MySqlCommand getCategoriesCommand = new MySqlCommand(ProcedureName, GetConnection());
            getCategoriesCommand.CommandType = System.Data.CommandType.StoredProcedure;

            return new List<Category>();
        }
    }
}
