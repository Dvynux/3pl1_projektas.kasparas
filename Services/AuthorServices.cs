﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Models;
using MySql.Data.MySqlClient;

namespace Services
{
    public class AuthorServices : BaseService, IDisposable
    {
        public AuthorServices()
        {

        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<Author> GetAuthors()
        {
            var cmdtext = "Get_Authors";

            using (var connection = GetConnection())
            {

                var Command = new MySqlCommand(cmdtext, GetConnection());
                Command.CommandType = System.Data.CommandType.StoredProcedure;
                //parametrai

                //duomenu ištraukimas ir aprdorojimas

                /*
                MySqlDataAdapter adapter = new MySqlDataAdapter(Command);

                DataTable dataTable = new DataTable();

                adapter.Fill(dataTable);

                var list = dataTable.AsEnumerable().Select(m => new Author
                (
                    m.Field<int>("id"), m.Field<string>("FirstName"), m.Field<string>("LastName"), m.Field<string>("Nationality")
                )).ToList();
                */

                var list = new List<Author>();

                var reader = Command.ExecuteReader();

                
                while (reader.Read())
                {
                    var author = new Author(
                        reader.GetString("FirstName"),
                        reader.GetString("LastName"),
                        reader.GetString("Nationality"));
                }
                list.Add(author);

                //duomenų gražinimas
                return list;

            }
        }
    }
}

