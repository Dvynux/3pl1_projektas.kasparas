﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = Dal.Models;
using MySql.Data.MySqlClient;
using FormModels = Dal.FormModels;
using Dal;

namespace Services
{
    public class BooksService : BaseService, IDisposable
    {
        private readonly LibraryDbContext _db;
        public BooksService()
        {
            _db = new LibraryDbContext();
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        //public List<Book> SearchBooks(BookSearch bookSearch)
        //{
        //    using (var connection = GetConnection())
        //    {
        //        var searchBooksCmdText = "Filter_books";
        //        var searchBooksCmd = new MySqlCommand(searchBooksCmdText, connection);
        //        searchBooksCmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        searchBooksCmd.Parameters.AddWithValue("InIsbn", string.IsNullOrWhiteSpace(bookSearch.Isbn) ? null : bookSearch.Isbn);
        //        searchBooksCmd.Parameters.AddWithValue("InTitle", string.IsNullOrWhiteSpace(bookSearch.Title) ? null : bookSearch.Title);
        //        searchBooksCmd.Parameters.AddWithValue("InAuthor", string.IsNullOrWhiteSpace(bookSearch.Author) ? null : bookSearch.Author);
        //        searchBooksCmd.Parameters.AddWithValue("InCategoryIds", "-1");

        //        MySqlDataAdapter adapter = new MySqlDataAdapter(searchBooksCmd);
        //        DataTable dataTable = new DataTable();
        //        adapter.Fill(dataTable);

        //        var list = dataTable.AsEnumerable().Select(m => new Book
        //        {
        //            BookId = m.Field<int>("Id"),
        //            Isbn = m.Field<string>("Isbn"),
        //            Title = m.Field<string>("Title"),
        //            DateOfPublish = m.Field<DateTime?>("DateOfPublish"),
        //            Categories = m.Field<string>("Categories")
        //        }).ToList();

        //        return list;
        //    }
        //}

        public List<FormModels.BookFilter> FilterBooks(Models.BookSearch bookSearch)
        {
            var data = (from book in _db.Books
                        let categories = _db.Categories.Join(_db.BooksByCategories,
                                            category => category.Id,
                                            bookByCategory => bookByCategory.CategoryId,
                                            (category, bookByCategory) => new { Category = category, BookByCategory = bookByCategory })
                                            .Where(m => m.BookByCategory.BookId == book.Id)
                                            .Select(m => m.Category.Name)
                                            .ToList()
                        let authors = _db.Authors.Join(_db.BooksByAuthors,
                                            author => author.Id,
                                            bookByAuthor => bookByAuthor.AuthorId,
                                            (author, bookByAuthor) => new { Author = author, BookByAuthor = bookByAuthor })
                                    .Where(m => m.BookByAuthor.BookId == book.Id)
                                    .Select(m => m.Author.Firstname + " " + m.Author.Lastname).ToList()
                        let booksAtLibraries = _db.Libraries.Join(_db.BooksAtLibraries,
                                           library => library.Id,
                                           bookAtLibrary => bookAtLibrary.LibraryId,
                                           (library, bookAtLibrary) => new { Library = library, BookAtLibrary = bookAtLibrary })
                                   .Where(m => m.BookAtLibrary.BookId == book.Id).Select(m => m.BookAtLibrary.Quantity).ToList()
                        let categoryIds = _db.BooksByCategories
                                                .Where(m => m.BookId == book.Id)
                                                .Select(m => m.CategoryId)
                                                .ToList()
                        select new
                        {
                            BookId = book.Id,
                            Title = book.Title,
                            Isbn = book.Isbn,
                            DateOfPublish = book.DateOfPublish,
                            Categories = categories,
                            Authors = authors,
                            BooksAtLibraries = booksAtLibraries,
                            CategoryIds = categoryIds
                        })
                        .AsEnumerable()
                        .Select(m => new FormModels.BookFilter
                        {
                            BookId = m.BookId,
                            Isbn = m.Isbn,
                            Title = m.Title,
                            DateOfPublish = m.DateOfPublish,
                            Categories = string.Join(", ", m.Categories),
                            Authors = string.Join(", ", m.Authors),
                            Quantity = m.BooksAtLibraries.Sum(),
                            CategoryIds = m.CategoryIds
                        }).Where(m => (bookSearch.Title == null || m.Title.Contains(bookSearch.Title ?? ""))
                                   && (bookSearch.Author == null || m.Authors.Contains(bookSearch.Author ?? ""))
                                   && (bookSearch.Isbn == null || m.Isbn == bookSearch.Isbn)).ToList();

            return data;
        }



        public List<FormModels.LibraryBookQuantity> GetBookQuantity(int bookId)
        {
            var data = (from library in _db.Libraries
                        join booksAtLibraries in _db.BooksAtLibraries on library.Id equals booksAtLibraries.LibraryId
                        

                        select new
                        {
                            Id = library.Id,
                            Name = library.Name,
                            Quantity = booksAtLibraries.Quantity,
                            BookId= booksAtLibraries.BookId
                        })
                        .AsEnumerable()
                        .Select(m => new FormModels.LibraryBookQuantity
                        {
                            Id = m.Id,
                            Name=m.Name,
                            Quantity=m.Quantity,
                            BookId=m.BookId
                        }).Where(m => (bookId == m.BookId))
                                   .ToList();

            return data;
        }
    }
}
