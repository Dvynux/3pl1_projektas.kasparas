﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Services;

namespace Presentation.Categories
{
    public partial class CategoryList : Form
    {
        private readonly CategoryService _service;

        public CategoryList()
        {
            InitializeComponent();
            _service = new CategoryService();
            FillGrid();
        }

        ~CategoryList()
        {
            _service.Dispose();
        }

        public void FillGrid()
        {
            var data = _service.GetCategories();
        }
    }
}
