﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dal.Models;
using Services;

namespace Presentation.Authors
{
    public partial class AuthorSave : Form
    {
        

        private readonly AuthorService _service;
        public AuthorSave(Author Author = null)
        {
            InitializeComponent();
            _service = new AuthorService();
            if (Author != null)
            {
                FillForm(Author);
            }
        }

        public void FillForm(Author Author)
        {
            txtId.Text = Author.Id.ToString();
            txtFirstName.Text = Author.Firstname;
            TxtLastName.Text = Author.Lastname;
            TxtNationality.Text = Author.Nationality;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                var id = string.IsNullOrWhiteSpace(txtId.Text) ? (int?)null : int.Parse(txtId.Text);
                var Author = new Author(id, txtFirstName.Text, TxtLastName.Text, TxtNationality.Text);

                _service.SaveAuthor(Author);
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show($"Nepavyko išsaugoti kategorijos.{ Environment.NewLine } { ex.Message.ToString()}");
            }
        }
    }
}
