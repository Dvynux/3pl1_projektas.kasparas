﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Services;

namespace Presentation.Categories
{
    public partial class AuthorList : Form
    {
        private readonly AuthorServices _service;
        
        public AuthorList()
        {
            InitializeComponent();
            _service = new AuthorServices();
            FillGrid();
        }

        ~AuthorList()
        {
            _service.Dispose();
        }

        public void FillGrid()
        {
            var data = _service.GetAuthors();
        }
    }
}
