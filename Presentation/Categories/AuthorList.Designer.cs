﻿namespace Presentation.Categories
{
    partial class AuthorList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.CategoryId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuthorFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuthorLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuthorNationality = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UpdateRecord = new System.Windows.Forms.DataGridViewButtonColumn();
            this.DeleteRecord = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnNewRecord = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CategoryId,
            this.AuthorFirstName,
            this.AuthorLastName,
            this.AuthorNationality,
            this.UpdateRecord,
            this.DeleteRecord});
            this.dataGridView1.Location = new System.Drawing.Point(12, 56);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(542, 103);
            this.dataGridView1.TabIndex = 2;
            // 
            // CategoryId
            // 
            this.CategoryId.HeaderText = "";
            this.CategoryId.Name = "CategoryId";
            this.CategoryId.ReadOnly = true;
            this.CategoryId.Visible = false;
            // 
            // AuthorFirstName
            // 
            this.AuthorFirstName.HeaderText = "FirstName";
            this.AuthorFirstName.Name = "AuthorFirstName";
            this.AuthorFirstName.ReadOnly = true;
            // 
            // AuthorLastName
            // 
            this.AuthorLastName.HeaderText = "LastName";
            this.AuthorLastName.Name = "AuthorLastName";
            this.AuthorLastName.ReadOnly = true;
            // 
            // AuthorNationality
            // 
            this.AuthorNationality.HeaderText = "Nationality";
            this.AuthorNationality.Name = "AuthorNationality";
            this.AuthorNationality.ReadOnly = true;
            // 
            // UpdateRecord
            // 
            this.UpdateRecord.HeaderText = "";
            this.UpdateRecord.Name = "UpdateRecord";
            this.UpdateRecord.ReadOnly = true;
            // 
            // DeleteRecord
            // 
            this.DeleteRecord.HeaderText = "";
            this.DeleteRecord.Name = "DeleteRecord";
            this.DeleteRecord.ReadOnly = true;
            // 
            // btnNewRecord
            // 
            this.btnNewRecord.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNewRecord.Location = new System.Drawing.Point(467, 12);
            this.btnNewRecord.Name = "btnNewRecord";
            this.btnNewRecord.Size = new System.Drawing.Size(75, 23);
            this.btnNewRecord.TabIndex = 3;
            this.btnNewRecord.Text = "New record";
            this.btnNewRecord.UseVisualStyleBackColor = true;
            // 
            // AuthorList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 195);
            this.Controls.Add(this.btnNewRecord);
            this.Controls.Add(this.dataGridView1);
            this.Name = "AuthorList";
            this.Text = "AuthorList";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn CategoryId;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuthorFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuthorLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuthorNationality;
        private System.Windows.Forms.DataGridViewButtonColumn UpdateRecord;
        private System.Windows.Forms.DataGridViewButtonColumn DeleteRecord;
        private System.Windows.Forms.Button btnNewRecord;
    }
}