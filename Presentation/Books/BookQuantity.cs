﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Services;

namespace Presentation.Books
{
    public partial class BookQuantity : Form
    {
        private readonly BooksService _service;
        public int BookId = BookSearch.ReadBook;
        public BookQuantity()
        {
            InitializeComponent();
            _service = new BooksService();
            FillGrid();


        }

        public void FillGrid()
        {
            var libraries = _service.GetBookQuantity(BookId);

            foreach (var library in libraries)
            {
                var rowIndex = dataGridView1.Rows.Add();
                dataGridView1.Rows[rowIndex].Cells["Id"].Value = library.Id.ToString();
                dataGridView1.Rows[rowIndex].Cells["LibraryName"].Value = library.Name;
                dataGridView1.Rows[rowIndex].Cells["Quantity"].Value = library.Quantity;
            }
        }


        private void BookQuantity_Load(object sender, EventArgs e)
        {

        }
    }
}
