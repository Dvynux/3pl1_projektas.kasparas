﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dal.FormModels;
using Services;
using Models = Dal.Models;

namespace Presentation.Books
{
    public partial class BookSearch : Form
    {
        private readonly BooksService _service;
        public BookSearch()
        {
            InitializeComponent();
            _service = new BooksService();
            FillGrid(new Models.BookSearch());
            listCategory.DisplayMember = "Name";
            listCategory.ValueMember = "Id";
        }

        public void FillGrid(Models.BookSearch bookSearch)
        {

            var data = _service.FilterBooks(bookSearch);

            dataGridView1.Rows.Clear();
            foreach (var book in data)
            {
                var index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells["BookId"].Value = book.BookId;
                dataGridView1.Rows[index].Cells["Isbn"].Value = book.Isbn;
                dataGridView1.Rows[index].Cells["Title"].Value = book.Title;
                dataGridView1.Rows[index].Cells["DateOfPublish"].Value = book.DateOfPublish.ToString();
                dataGridView1.Rows[index].Cells["Categories"].Value = book.Categories;
                dataGridView1.Rows[index].Cells["Authors"].Value = book.Authors;
                dataGridView1.Rows[index].Cells["Quantity"].Value = book.Quantity.ToString();
            }
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            var searchModel = new Models.BookSearch
            {
                Isbn = string.IsNullOrEmpty(txtIsbn.Text) ? null : txtIsbn.Text,
                Title = string.IsNullOrEmpty(txtTitle.Text) ? null : txtTitle.Text,
                Author = string.IsNullOrEmpty(txtAuthor.Text) ? null : txtAuthor.Text,
                Categories = new List<int>()
            };

            foreach (var item in listCategory.Items)
            {
                var category = (Category)item;
                searchModel.Categories.Add(category.Id.Value);
            }

            FillGrid(searchModel);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            CategorySelect categorySelect = new CategorySelect();
            categorySelect.FormClosed += CategorySelect_FormClosed;
            categorySelect.ShowDialog();
        }

        private void CategorySelect_FormClosed(object sender, FormClosedEventArgs e)
        {
            var form = (CategorySelect)sender;
            var categories = form.Categories;

            listCategory.Items.Clear();

            foreach (var category in categories)
            {
                listCategory.Items.Add(category);
            }
        }

        private void BtnClearCategories_Click(object sender, EventArgs e)
        {
            listCategory.Items.Clear();
        }

        public static int ReadBook;
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                ReadBook = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells["BookId"].Value);

                BookQuantity bookQuantity = new BookQuantity();
                bookQuantity.FormClosed += BookQuantity_FormClosed;
                bookQuantity.ShowDialog();
            }
        }

        private void BookQuantity_FormClosed(object sender, FormClosedEventArgs e)
        {
            //throw new NotImplementedException();
        }
    }
}
