﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Services;

namespace Presentation.Books
{
    public partial class Book_Quantity : Form
    {
        private readonly BooksService _service;
        public Book_Quantity()
        {
            InitializeComponent();
            _service = new BooksService();
            FillGrid();
        }
       

        ~Book_Quantity()
        {
            _service.Dispose();
        }

        public void FillGrid()
        {
            var categories = _service.GetBookQuantity();

            foreach (var category in categories)
            {
                var rowIndex = dataGridView1.Rows.Add();
                dataGridView1.Rows[rowIndex].Cells["Book_Id"].Value = category.Id.ToString();
                dataGridView1.Rows[rowIndex].Cells["Isbn"].Value = category.Isbn;
                dataGridView1.Rows[rowIndex].Cells["Quantity_of_books"].Value = category.Quantity_in_stock;
            }
        }
        
    }
}
