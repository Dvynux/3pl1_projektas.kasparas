﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Models
{
    public class BookLibrary
    {
        public int Id { get; set; }
        public string Isbn { get; set; }
        public int Quantity_in_stock { get; set; }

        public BookLibrary(int id, string isbn, int quantity_in_stock)
        {
            Id = id;
            Isbn = isbn;
            Quantity_in_stock = quantity_in_stock;
        }
    }

}
