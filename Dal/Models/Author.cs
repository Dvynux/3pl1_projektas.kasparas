﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Models
{
    [Table("authors")]
    public class Author
    {
        [Key]
        public int Id { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Nationality { get; set; }

        private Author() { }
        
        public Author(string firstname, string lastname, string nationality)
        {
            Firstname = firstname;
            Lastname = lastname;
            Nationality = nationality;
        }
    }
}
