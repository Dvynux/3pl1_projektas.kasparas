﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Models
{
    [Table("booksatlibraries")]
    public class BooksAtLibraries
    {
        [Key]
        [Column(Order = 1)]
        public int BookId { get; private set; }

        [Key]
        [Column(Order = 2)]
        public int LibraryId { get; private set; }
        public int quantity { get; set; }
        public BooksAtLibraries(){}

        public BooksAtLibraries(int quantity)
        {
            this.quantity = quantity;
        }
    }
}
