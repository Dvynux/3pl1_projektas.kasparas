﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.FormModels
{
    public class LibraryBookQuantity
    {
        public int? Id { get; set; }

        public int? BookId { get; set; }

        public string Name { get; set; }

        public int Quantity { get; set; }
    }
}
